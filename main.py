def swap_case(s):
  
  out = ''
  ch = ''
  for i in range(len(s)):
    ch = s[i]
    if(ch.isalpha()):
      if (ch.islower()):
        out += ch.upper()
      else:
        out += ch.lower()
    else:
      out += ch
  
  return out

print(swap_case('tYu2:sZar'))  
